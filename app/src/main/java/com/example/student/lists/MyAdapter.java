package com.example.student.lists;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyAdapter extends BaseAdapter {

    private Context context;
    private List<User> users = new ArrayList<>();

    public MyAdapter(Context context, @NonNull List<User> users) {
        this.context = context;
        this.users = users;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public User getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return users.get(position).hashCode();
    }

    @Override
    public View getView(int position, View conveertView, ViewGroup parent) {

        View rowView = conveertView;
        ViewHolder holder;
        if (rowView == null){
            rowView = LayoutInflater.from(context)
                    .inflate(R.layout.item_user,parent, false);
            holder = new ViewHolder(rowView);
//            holder.icon = rowView.findViewById(R.id.icon);
//            holder.userName = rowView.findViewById(R.id.user_name);
//            holder.phone = rowView.findViewById(R.id.phone);

            rowView.setTag(holder);
        } else{
            holder = (ViewHolder) rowView.getTag();
        }


        holder.icon.setImageResource(getItem(position).getIcon());
        holder.userName.setText(getItem(position).getName());
        holder.phone.setText(getItem(position).getPhone());
        return rowView;
    }


    static class ViewHolder {
        @BindView(R.id.icon)
                ImageView icon;
        @BindView(R.id.user_name)
                TextView userName;
        @BindView(R.id.phone)
                TextView phone;

        public ViewHolder(View root) {
            ButterKnife.bind(this, root);
        }
    }
}
