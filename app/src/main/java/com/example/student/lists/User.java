package com.example.student.lists;

public class User {
    private String name;
    private String phone;
    private int icon;

    public User(String name, String phone, int icon) {
        this.name = name;
        this.phone = phone;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public int getIcon() {
        return icon;
    }
}
