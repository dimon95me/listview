package com.example.student.lists;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnItemClick;

public class MainActivity extends AppCompatActivity {

    MyAdapter myAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        myAdapter = new MyAdapter(this, generateUsers());
        ListView listView = findViewById(R.id.list);
        listView.setAdapter(myAdapter);

    }

    @OnItemClick(R.id.list)
    public void onItemClick(int poosition){
        Toast.makeText(MainActivity.this, myAdapter.getItem(poosition).getName(), Toast.LENGTH_SHORT).show();
    }

    public List<User> generateUsers(){
        List<User> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            list.add(new User("Vasya"+i, "+38057675"+(i%10),R.mipmap.ic_launcher));
        }
        return list;
    }
}
